package com.example.demo;

import com.amazonaws.auth.ProcessCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.util.logging.Logger;

public class ProcessExecProvider implements IProvider {

    Logger logger = Logger.getLogger(ProcessExecProvider.class.getName());

    @Override
    public AmazonS3 getS3Client(String region) {
        logger.info("Building S3 Client with ProcessExec credentials provider");
        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(ProcessCredentialsProvider.builder().withCommand("/usr/local/bin/aws-vault exec myprofile --json --prompt=osascript").build())
                .build();
        return s3;
    }
}
