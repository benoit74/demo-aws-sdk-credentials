package com.example.demo;

import com.amazonaws.services.s3.AmazonS3;

public interface IProvider {

    AmazonS3 getS3Client(String region);

}
