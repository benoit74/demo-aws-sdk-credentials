package com.example.demo;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.util.logging.Logger;

public class DefaultProvider implements IProvider {

    Logger logger = Logger.getLogger(DefaultProvider.class.getName());

    @Override
    public AmazonS3 getS3Client(String region) {
        logger.info("Building S3 Client without credentials provider");
        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .build();
        return s3;
    }

}
