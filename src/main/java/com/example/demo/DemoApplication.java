package com.example.demo;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.List;
import java.util.logging.Logger;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	Logger logger = Logger.getLogger(DemoApplication.class.getName());

	@Autowired
	IProvider executor;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	@Profile("local")
	public IProvider getDefaultProvider() {
		return new DefaultProvider();
	}

	@Bean
	@Profile("!local")
	public IProvider getProcessExecProvider() {
		return new ProcessExecProvider();
	}

	@Override
	public void run(String... args) throws Exception {

		// Add your application logic here
		logger.info("Hello Spring Boot from console!");
		AmazonS3 s3 = executor.getS3Client("eu-west-1");
		List<Bucket> bucketList = s3.listBuckets();
		logger.info(String.format("%d buckets found", bucketList.size()));
		System.exit(0);
	}

}
