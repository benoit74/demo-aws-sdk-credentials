This application is a demo of the way to source credentials to use with the various AWS SDK.

In this demo this code is trying to count the number of buckets in our AWS account.

If you specify the 'local' profile, no credentials are provided to the AWS S3 client at its creation.

If you do not specify the 'local' profile, the ProcessCredentialsProvider is provided to the AWS S3 client at its creation. 
In this demo, the process ran is '/usr/local/bin/aws-vault exec myprofile --json --prompt=osascript', which you should 
probably adapt to match your profile name (you probably do not have any 'myprofile' in your configuration).
